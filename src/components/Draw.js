import VectorLayer from "ol/layer/Vector.js";
import VectorSource from "ol/source/Vector.js";
import { Draw, Modify, Snap } from "ol/interaction.js";
import { Icon, Style } from "ol/style.js";

export default class {
    constructor(map, onDrawEnd, onModify) {
        this.drawSource = new VectorSource();
        this.map = map;
        var drawLayer = new VectorLayer({
            source: this.drawSource,
            style: function (feature) {
                return new Style({
                    image: new Icon(
                  /** @type {module:ol/style/Icon~Options} */({
                            anchor: [0.5, 32],
                            scale: 1.4,
                            anchorXUnits: "fraction",
                            anchorYUnits: "pixels",
                            src: "images/markers/green.png"
                        })
                    )
                });
            }
        });
        this.map.addLayer(drawLayer);
        this.createInteractions(this.drawSource);
        this.addInteractions(this.drawSource, onDrawEnd, onModify);
    }

    createInteractions(source) {
        this.modify = new Modify({ source: source });
        this.modify.setActive(false);
       

        this.draw = new Draw({
            source: source,
            type: "Point"
        });

        this.snap = new Snap({ source: source });
    }

    addInteractions(source, onDrawEnd, onModify) {
        this.draw.on("drawend", e => {
            this.draw.setActive(false);
            this.modify.setActive(true);
            onDrawEnd(e)
        });
        this.modify.on("modifyend", onModify);
        this.map.addInteraction(this.modify);
        this.map.addInteraction(this.draw);
        this.draw.setActive(false);
        this.map.addInteraction(this.snap);
    }

    startEdit() {
        if (this.drawActive) {
            this.modify.setActive(false);
            this.draw.setActive(false);
        } else {
            this.modify.setActive(false);
            this.draw.setActive(true);
        }
    }

    stopEdit() {
        this.modify.setActive(false);
        this.draw.setActive(false);
    }

    get drawActive() {
        return (
            typeof this.draw != "undefined" &&
            (this.draw.getActive() || this.modify.getActive())
        );
    }
}