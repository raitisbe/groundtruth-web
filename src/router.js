import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Observations from './views/Observations.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/observations', component: Observations },
    { path: '/about', component: () => import(/* webpackChunkName: "about" */ './views/About.vue') },
    { path: '/signin', component: () => import(/* webpackChunkName: "about" */ './views/Signin.vue') },
    { path: '/signup', component: () => import(/* webpackChunkName: "about" */ './views/Signup.vue') }
  ]
})
