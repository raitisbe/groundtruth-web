import Vue from 'vue'
import Vuetify from 'vuetify'
import VueScrollTo from 'vue-scrollto';
import App from './App.vue'
import router from './router'
import axios from 'axios'

const baseURL = process.env.NODE_ENV === "development"
  ? 'https://localhost:10222'
  : "https://groundtruth.plan4all.eu"

axios.defaults.baseURL = baseURL
axios.defaults.withCredentials = true


Vue.config.productionTip = false
Vue.use(Vuetify)
Vue.use(VueScrollTo)


export const EventBus = new Vue();
export let globalStore = {layers:[], siteConfig:{}, observationTypes:[]};

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
