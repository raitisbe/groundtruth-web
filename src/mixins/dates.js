import moment from 'moment'

export default {
    methods: {
        formatDateTime: function (value) {
            return moment(value).format('DD.MM.YYYY HH:mm')
        }
    }
}