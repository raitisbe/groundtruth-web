const keystone = require('keystone');
const moment = require('moment');
const async = require('async');
const Vote = keystone.list('Vote');
const Observation = keystone.list('Observation');

/**
* Create a Vote
*/
exports.create = function (req, res) {
    let item = new Vote.model();
    let data = (req.method == 'POST') ? req.body : req.query;
    data.date = data.date ? new Date(data.date) : new Date();
    data.value = Math.sign(data.value);
    if (req.user) data.user = req.user;
    async.waterfall([
        cb => {
            Observation.model.findById(data.observation).populate('observationType').exec(function (err, record) {
                if (record) {
                    if (record.observationType && record.observationType.voteType == 'numerical')
                        data.value = data.numericValue;
                    cb(null, { data, item })
                } else {
                    cb('No record found')
                }
            })
        },
        (ctx, cb) => {
            cb(null, ctx)
        },
        (ctx, cb) => {
            ctx.item.getUpdateHandler(req).process(ctx.data, (err) => {
                cb(err, ctx)
            });
        }
    ], (err, ctx) => {
        if (err) return res.apiError('error', err);
        if (typeof ctx.item.user != 'undefined') delete ctx.item.user;
        res.apiResponse(ctx.item);
    })
}
