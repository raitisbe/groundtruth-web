const async = require('async'),
    keystone = require('keystone'), moment = require('moment'),
    sharp = require('sharp');

const Observation = keystone.list('Observation');
const Vote = keystone.list('Vote');
const User = keystone.list('User');
const relation_columns = '';
const storageDirectory = 'storage';

/**
* List Observations
*/
exports.list = function (req, res) {

    async.parallel({
        items: (cb) => Observation.model.find({ user: req.user })
            .sort([['date', -1]])
            .populate('observationType')
            .select('_id date name observationType screenshot scope')
            .exec().then((items) => cb(null, items))
    }, (err, results) => {
        let json = results.items.map(function (p) {
            let item = p.toJSON();
            return item;
        })
        res.apiResponse(json);
    })
}

function accessFilter(req) {
    let tmp = {
        $or: [
            { scope: 'public' }
        ]
    }
    if (req.user) {
        tmp.$or.push({ user: req.user });
        tmp.$or.push({ user: { $in: req.user.friends } });
    }
    return tmp;
}

/**
* List Observations for me and friends
*/
exports.list4Home = function (req, res) {
    async.waterfall([
        (cb) => Observation.model.find(accessFilter(req)).populate('user observationType')
            .select('_id date user name observationType description center screenshot image scope')
            .sort([['date', -1]])
            .limit(10)
            .exec().then((items) => cb(null, { observations: items })),
        (ctx, cb) => {
            const observationIds = ctx.observations.map(r => r._id.toString());
            Vote.model.find({ observation: { $in: observationIds } }).populate('user').exec().then(
                (votes) =>
                    cb(null, Object.assign(ctx, { votes: votes }))
            ).catch(err => console.error(err.message, err.stack))
        }
    ], (err, results) => {
        let json = results.observations.map(function (p) {
            let item = p.toJSON();
            if (item.user) {
                item.user = {
                    _id: item.user._id,
                    nick: p.user.nick,
                    avatar: User.getAvatar(item.user)
                }
            }
            item.votes = results.votes.filter(vote => vote.observation == item._id.toString());
            item.confirmedCount = item.votes.filter(vote => vote.value == 1).length;
            item.deniedCount = item.votes.filter(vote => vote.value == -1).length;
            item.confirmed = item.votes.filter(
                vote => typeof req.user != 'undefined' && vote.user._id.toString() == req.user._id.toString() && vote.value == 1
            ).length > 0;
            item.denied = item.votes.filter(
                vote => typeof req.user != 'undefined' && vote.user._id.toString() == req.user._id.toString() && vote.value == -1
            ).length > 0;
            return item;
        })
        res.apiResponse(json);
    })
}

/**
* Get Observation by ID
*/
exports.get = function (req, res) {
    Observation.model.findById(req.params.id).populate('user')
        .select('_id date user name description observationType screenshot image')
        .exec(function (err, record) {

            if (err) return res.apiError('database error', err);
            if (!record) {
                return res.apiError('not found');
            }
            let item = record.toJSON();
            if (item.user) {
                item.user = {
                    _id: item.user._id,
                    name: record.user.name.full,
                    avatar: User.getAvatar(item.user)
                }
            }
            res.apiResponse(item);
        });
}


/**
* Create a Observation
*/
exports.create = function (req, res) {

    var item = new Observation.model(),
        data = (req.method == 'POST') ? req.body : req.query;
    prepareData(data);
    if (data.date) data.date = new Date(data.date); else data.date = new Date();
    if (req.user) data.user = req.user;
    async.waterfall([
        cb => {
            cb(null, data, item._id, 'screenshot')
        },
        processImage,
        (data, cb) => {
            cb(null, data, item._id, 'image')
        },
        processImage
    ], (err, data) => {
        item.getUpdateHandler(req).process(data, function (err) {
            if (err) return res.apiError('error', err);
            if (typeof item.user != 'undefined') delete item.user;
            res.apiResponse(item);

        });
    })
}

function processImage(data, id, property, cb) {
    try {
        if (data[property]) {
            let format = null;
            if (data[property].indexOf('png;base64') > -1) format = 'png';
            if (data[property].indexOf('jpeg;base64') > -1) format = 'jpeg';
            if (format == null) cb(null, data);
            var base64Data = data[property].replace(/^data:image\/png;base64,/, "");
            base64Data = base64Data.replace(/^data:image\/jpeg;base64,/, "");
            var tmp = `/images/observations/${id}${property}.${format}`;
            require("fs").writeFile(`${storageDirectory}${tmp}`, base64Data, 'base64', function (err) {
                try {
                    data[property] = `/images/observations/${id}${property}_resized.${format}`;
                    sharp(`${storageDirectory}${tmp}`)
                        .resize({ width: 600, height: 400, fit: sharp.fit.cover })
                        .toFile(`${storageDirectory}${data[property]}`)
                        .then((ImageResult) => {
                            cb(null, data)
                        })
                } catch (ex) {
                    cb(ex, data)
                }
            });
        } else
            cb(null, data)
    } catch (ex) {
        cb(ex, data)
    }
}

function prepareData(data) {
    if (data._id) delete data._id;
    var imagesDir = `${storageDirectory}/images`;
    createDirIfNeeded(storageDirectory)
    createDirIfNeeded(imagesDir);
    createDirIfNeeded(`${imagesDir}/observations`);
    if (data.wkt == null) delete data.wkt;
    if (data.image == null || data.image == '') delete data.image;
    if (data.center == null || data.center.geo == null) delete data.center;
    if (data.screenshot == null || data.screenshot=='') delete data.screenshot;
}

function createDirIfNeeded(dir) {
    var fs = require('fs');
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
}

/**
* Get Observation by ID
*/
exports.update = function (req, res) {
    Observation.model.findById(req.params.id).exec(function (err, item) {

        if (err) return res.apiError('database error', err);
        if (!item) return res.apiError('not found');
        if (item.user && item.user != req.user._id.toString()) return res.apiError('Access denied');
        var data = (req.method == 'POST') ? req.body : req.query;
        prepareData(data);

        async.waterfall([
            cb => {
                cb(null, data, item._id, 'screenshot')
            },
            processImage,
            (data, cb) => {
                cb(null, data, item._id, 'image')
            },
            processImage,
            (data, cb) => {
                item = Object.assign(item, data);
                cb(null, item)
            }
        ], (err, item) => {
            item.save().then(() => {
                res.apiResponse({ result: 1 });
            });
        })
    });
}

/**
* Delete Observation by ID
*/
exports.remove = function (req, res) {
    Observation.model.findById(req.params.id).exec(function (err, item) {

        if (err) return res.apiError('database error', err);
        if (!item) return res.apiError('not found');
        if (item.user && item.user != req.user._id.toString()) return res.apiError('Access denied');

        item.remove(function (err) {
            if (err) return res.apiError('database error', err);

            return res.apiResponse({
                success: true
            });
        });

    });
}