var async = require('async'),
    keystone = require('keystone'), moment = require('moment');

const SiteConfig = keystone.list('SiteConfig');
const ObservationType = keystone.list('ObservationType');
const Layer = keystone.list('Layer');

/**
* List config
*/
exports.list = function (req, res) {
    async.parallel({
        siteConfig: (cb) => SiteConfig.model.find({})
            .exec().then((items) => cb(null, items)),
        observationTypes: (cb) => ObservationType.model.find({})
            .exec().then((items) => cb(null, items)),
        layers: (cb) => Layer.model.find({})
            .exec().then((items) => cb(null, items))
    }, (err, results) => {
        let user = JSON.parse(JSON.stringify(req.user));
        delete user.password;
        res.apiResponse({
            siteConfig: results.siteConfig[0],
            observationTypes: results.observationTypes,
            layers: results.layers,
            user: user
        });
    })
}
