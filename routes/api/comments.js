const keystone = require('keystone');
const moment = require('moment');
const async = require('async');
const Comment = keystone.list('Comment');
const User = keystone.list('User');

/**
* Create a Comment
*/
exports.create = function (req, res) {
    let item = new Comment.model();
    let data = (req.method == 'POST') ? req.body : req.query;
    data.date = data.date ? new Date(data.date) : new Date();
    if (req.user) data.user = req.user;
    if (typeof data.content == 'undefined' || data.content == '' || data.content == null) {
        res.apiResponse(null);
    }
    async.waterfall([
        cb => {
            cb(null, { data, item })
        },
        (ctx, cb) => {
            ctx.item.getUpdateHandler(req).process(ctx.data, (err) => {
                cb(err, ctx)
            });
        }
    ], (err, ctx) => {
        if (err) return res.apiError('error', err);
        if (typeof ctx.item.user != 'undefined') delete ctx.item.user;
        res.apiResponse(ctx.item);
    })
}

/**
* List comments
*/
exports.listForObservation = function (req, res) {
    Comment.model.find({ observation: req.params.observation })
        .populate('user')
        .sort([['date', 1]])
        .select('_id date content')
        .exec().then((items) => {
            let json = items.map(function (p) {
                let item = p.toJSON();
                if(item.user) {
                    item.user = {
                        _id: item.user._id,
                        name: p.user.name.full,
                        avatar: User.getAvatar(item.user)
                    }
                }
                return item;
            })
            res.apiResponse(json);
        }).catch(err => console.error(err.message, err.stack))
}
