var async = require('async'),
    keystone = require('keystone'),
    moment = require('moment'),
    request = require('request');

var User = keystone.list('User');
var relation_columns = '';

/**
* List Users
*/
exports.list = function (req, res) {

    async.parallel({
        items: (cb) => User.model.find({ _id: { $ne: req.user._id.toString() } }).exec().then((items) => cb(null, items)),
    }, (err, results) => {
        let json = results.items.map(function (u) {
            let item = u.toJSON();
            return item;
        })
        res.apiResponse(json);
    })
}

function slimUser(u) {
    let item = u.toJSON();
    item.name = item.name.first + ' ' + item.name.last;
    item.avatar = User.getAvatar(item);
    delete item.services;
    return item;
}

exports.friendsList = function (req, res) {
    async.parallel({
        items: (cb) => User.model.find({ _id: { $in: req.user.friends } })
            .select('name avatar services _id')
            .sort('name')
            .exec().then((items) => cb(null, items)),
    }, (err, results) => {
        let json = results.items.map(slimUser)
        res.apiResponse(json);
    })
}

exports.friendsAdd = function (req, res) {
    if (req.user.friends.indexOf(req.params.id) == -1) {
        req.user.friends = req.user.friends.concat(req.params.id);
        req.user.save().then(() => {
            res.apiResponse({ result: "OK" });
        })
    }
    else
        res.apiResponse({ result: "OK" });
}

exports.friendsFind = function (req, res) {
    let filters = req.params.filter.split(' ').map(part => {
        return {
            $or: [
                { "name.first": { $regex: part, $options: "i" } },
                { "name.last": { $regex: part, $options: "i" } }
            ]
        }
    });
    User.model.find({ $and: filters })
        .select('name avatar services _id')
        .sort('name')
        .exec().then((items) => {
            let json = items.map(slimUser)
            res.apiResponse(json);
        }
        ).catch(err => console.error(err.message, err.stack))
}

/**
* Get User by ID
*/
exports.get = function (req, res) {
    User.model.findById(req.params.id).populate(relation_columns).exec(function (err, item) {

        if (err) return res.apiError('database error', err);
        if (!item) return res.apiError('not found');

        res.apiResponse(item);

    });
}

/**
* Create a User
*/
exports.create = function (req, res) {

    var item = new User.model(),
        data = (req.method == 'POST') ? req.body : req.query;
    prepareData(data);
    item.getUpdateHandler(req).process(data, function (err) {

        if (err) return res.apiError('error', err);

        res.apiResponse(item);

    });
}

function prepareData(data) {
    if (data._id) delete data._id;
}

/**
* Get User by ID
*/
exports.update = function (req, res) {
    User.model.findById(req.params.id).exec(function (err, item) {

        if (err) return res.apiError('database error', err);
        if (!item) return res.apiError('not found');
        if (item._id.toString() != req.user._id.toString()) return res.apiError('Access denied');

        var data = (req.method == 'POST') ? req.body : req.query;
        prepareData(data);
        item.getUpdateHandler(req).process(data, function (err) {
            if (err) return res.apiError('create error', err);
            res.apiResponse(item);
        });

    });
}

/**
* Delete User by ID
*/
exports.remove = function (req, res) {
    User.model.findById(req.params.id).exec(function (err, item) {

        if (err) return res.apiError('database error', err);
        if (!item) return res.apiError('not found');
        if (item._id.toString() != req.user._id.toString()) return res.apiError('Access denied');

        item.remove(function (err) {
            if (err) return res.apiError('database error', err);

            return res.apiResponse({
                success: true
            });
        });

    });
}

exports.populateFriends = function (user, accessToken, cb) {
    request({
        uri: 'https://graph.facebook.com/me/friends?access_token=' + accessToken,
        method: 'GET',
        json: true
    }, function (error, response, body) {
        var friend_ids = body.data.map(f => f.id);
        User.model.find({ "services.facebook.profileId": { "$in": friend_ids } }).exec().then((fb_friends) => {
            var new_fb_friends = fb_friends.filter(fb_friend => user.friends.indexOf(fb_friend._id) == -1);
            if (new_fb_friends.length == 0)
                cb(null);
            else {
                user.friends = user.friends.concat(new_fb_friends.map(fb_friend => fb_friend._id));
                user.save().then(() => cb(null))
            }
        })

    })
}
