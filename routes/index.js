var keystone = require('keystone');
var middleware = require('./middleware');
var importRoutes = keystone.importer(__dirname);
var bodyParser = require('body-parser');
var path = require('path');
var cors = require('cors')({
    origin: 'http://localhost:8080',
    credentials: true });

// Common Middleware
keystone.pre('routes', middleware.initLocals);
keystone.pre('render', middleware.flashMessages);

// Import Route Controllers
var routes = {
	views: importRoutes('./views'),
	api: importRoutes('./api')
};

var mdl_api = keystone.middleware.api;
var mdl_auth = middleware.requireUser;
var jsonParser = bodyParser.json({ limit: '10mb' });

// Setup Route Bindings
exports = module.exports = function (app) {
	// Views
	app.get('/', middleware.requireUser, routes.views.index);

	app.all('/user/signup', routes.views.auth.signup);
	app.all('/signin', routes.views.auth.signin);
	app.all('/signout', routes.views.auth.signout);
	app.all('/forgot-password', routes.views.auth['forgot-password']);
	app.all('/reset-password/:key', routes.views.auth['reset-password']);
	app.all('/join', routes.views.auth.join);
	app.all('/auth/confirm', routes.views.auth.confirm);
	app.all('/auth/:service', routes.views.auth.service);

	app.all('/app-position/google4c1bc0d703d16b8e.html', function (req, res) {
		res.sendFile(path.join(__dirname + '/../templates/views/google4c1bc0d703d16b8e.html'));
	});

	app.all('/privacy_policy', function (req, res) {
		res.sendFile(path.join(__dirname + '/../templates/views/privacy_policy.html'));
	});

	app.all('/api/*', cors);
	app.all('/api/*', mdl_api);
	app.get('/api/friends/list', [mdl_auth], routes.api.users.friendsList);
	app.get('/api/friends/find/:filter', [mdl_auth], routes.api.users.friendsFind);
	app.get('/api/friends/add/:id', [mdl_auth], routes.api.users.friendsAdd);

	app.all('/api/auth/authorized', routes.views.auth.api.authorized);
	app.all('/api/auth/signout', routes.views.auth.api.signout);
	app.all('/api/auth/signup', routes.views.auth.api.signup);
	app.all('/api/auth/signin', routes.views.auth.api.signin);
	app.post('/api/auth/fbsignin/', routes.views.auth.api.fbsignin);

	app.get('/api/observations/list', [], routes.api.observations.list);
	app.get('/api/observations/list_home', [], routes.api.observations.list4Home);
	app.all('/api/observations/create', [jsonParser, mdl_auth], routes.api.observations.create);
	app.all('/api/observations/:id/update', [jsonParser, mdl_auth], routes.api.observations.update);
	app.get('/api/observations/:id/remove', [mdl_auth], routes.api.observations.remove);
	app.get('/api/observations/:id', routes.api.observations.get);

	app.all('/api/votes/create', [mdl_auth], routes.api.votes.create);
	app.all('/api/comments/create', [mdl_auth], routes.api.comments.create);
	app.all('/api/comments/list/:observation', [mdl_auth], routes.api.comments.listForObservation);

	app.get('/api/config/list', [mdl_auth], routes.api.config.list);

};
