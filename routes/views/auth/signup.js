// @file signup.js
// @path /routes/views/signup.js
// @description Handles the post request when the user tries to sign up.
// @url https://github.com/keystonejs/generator-keystone/issues/10
//
var keystone = require('keystone');
var User = keystone.list('User');
const reCAPTCHA = require('recaptcha2');
const reCaptchaPublicKey = '6LcfphkUAAAAAGZXaq6T61kwFuYxd3G9wLmCa6qD';
const reCaptchaPrivateKey = '6LcfphkUAAAAAFFE2lN156IPa0fUqC7ZhXYKEVws';
//const reCaptchaPublicKey = '6Le-8x4UAAAAAJMYy_54nWrUkF37bAQPmcdGXMVU';
//const reCaptchaPrivateKey = '6Le-8x4UAAAAAItXOtBsG9wZXuXBjo3_uZuNQ3A9';

exports = module.exports = function (req, res) {

    var view = new keystone.View(req, res);
    var locals = res.locals;
    const recaptcha = new reCAPTCHA({
        siteKey: reCaptchaPublicKey,
        secretKey: reCaptchaPrivateKey
    });
    // Set locals
    locals.section = 'signup';
    locals.filters = {
    };
    locals.data = {
        captchakey: reCaptchaPublicKey
    };

    //console.log(req);

    locals.formData = req.body || {};

    view.on('post', { action: 'user.create' }, function (next) {

        // if (!keystone.security.csrf.validate(req)) {
        // 	req.flash('error', 'There was an error with your request, please try again.');
        // 	return renderView();
        // }

        if (locals.formData.password !== locals.formData.password_confirm) {
            req.flash('error', 'The passwords do not match.');
            next();
            return;
        }

        var onSuccess = function () {
            try {
                console.log('redirecting to /');
                res.redirect('/');
            } catch (ex) {
                console.log(ex);
            }
        }

        var onFail = function (result) {
            try {
                req.flash('error', result.error);
                locals.data.validationErrors = result.errors;
                next();
            } catch (ex) {
                console.log(ex);
            }
        }

        recaptcha.validate(locals.formData['g-recaptcha-response'])
            .then(() => {
                signUp(locals.formData.first, locals.formData.last, locals.formData.email, locals.formData.password, onSuccess, onFail, req, res)
            })
            .catch(err => {
                req.flash('error', 'Captcha verification failed.');
                next();
            });
    });   

    view.render('signup');
};

function signUp(first, last, email, password, onSuccess, onFail, req, res){
    var newUser = new User.model({
        name: {
            first: first,
            last: last
        },
        email: email,
        password: password,
        approved: true
        //Add some user defaults here.
    });

    newUser.isAdmin = false;

    newUser.save(function (err, result) {
        if (err) {
            if (err.code == 11000) {
                onFail({result: false, error: 'User with this e-mail already exists!'});
            } else {
                onFail({result: false, errors: err.errors});
            }
        } else {
            keystone.session.signin({ email: email, password: password }, req, res, onSuccess, onFail);                 
        }
    });
}

