var keystone = require('keystone'),
    async = require('async'),
    api_users = require('../../api/users');
var User = keystone.list('User');

exports.authorized = function (req, res) {
    if (req.user) {
        res.apiResponse({ result: true, user: { name: req.user.name, services: req.user.services } });
    } else {
        res.apiResponse({ result: false });
    }
}


exports.signout = function (req, res) {
    keystone.session.signout(req, res, function() {
		res.apiResponse({ result: true });
	});
}

function signUp(first, last, email, password, onSuccess, onFail, req, res){
    var newUser = new User.model({
        name: {
            first: first,
            last: last
        },
        email: email,
        password: password,
        approved: true,
        isAdmin: false
    });

    newUser.save(function (err, result) {
        if (err) {
            if (err.code == 11000) {
                onFail({result: false, error: 'User with this e-mail already exists!'});
            } else {
                onFail({result: false, errors: err.errors});
            }
        } else {
            keystone.session.signinWithUser(newUser, req, res, function () {
                req.session.user = newUser;
                onSuccess();
            })         
        }
    });
}

exports.signup = function (req, res) {
    let body = req.body;
    signUp(body.first, body.last, body.email, body.password, function () {
        res.apiResponse({ result: true });
    }, function(result) {
        res.apiResponse(result);
    }, req, res)
}

exports.signin = function (req, res) {
    let body = req.body;
    keystone.session.signin({ email: body.email, password: body.password }, req, res, () => {
        let options = {
            maxAge: 1000 * 60 * 15, // would expire after 15 minutes
            httpOnly: true, // The cookie only accessible by the web server
            signed: true // Indicates if the cookie should be signed
        }
    
        // Set cookie
        res.cookie('cookieName', 'cookieValue', options);
        res.apiResponse({ result: true });
    }, () => {
        res.apiResponse({ result: false });
    })
}

exports.fbsignin = function (req, res) {
    var request = require('request');

    async.waterfall([
        (cb) => {
            request({
                uri: 'https://graph.facebook.com/me?fields=name,email&access_token=' + req.body.access_token,
                method: 'GET',
                json: true
            }, function (error, response, body) {
                if (error) { cb({ result: 'some error' }, null); return }
                if (typeof body != 'undefined' && body != null && typeof body.id != 'undefined') {
                    cb(null, { body: body })
                } else {
                    cb({ result: 'something was not right. check if the accesstoken is specified and there exists a user for this facebook profile' }, null);
                    return;
                }
            });
        },
        (ctx, cb) => {
            if (ctx==null) { cb(err, {}); return; }
            User.model.findOne({ $or: [{ 'services.facebook.profileId': ctx.body.id }, { email: ctx.body.email }] }).select('-password -__v').exec()
                .then(function (data) {
                    if (data != null) {
                        keystone.session.signinWithUser(data, req, res, function () {
                            req.session.user = data;
                            ctx.token = req.sessionID;
                            cb(null, ctx);
                        })
                        if (typeof data.services.facebook.profileId == 'undefined' || data.services.facebook.profileId == null) {
                            data.services.facebook.profileId = ctx.body.id;
                            data.services.facebook.isConfigured = true;
                            data.save();
                        }
                    } else {
                        var first = '';
                        var last = '';
                        if (ctx.body.name.length > 0) {
                            var parts = ctx.body.name.split(' ');
                            first = parts.shift();
                            if (parts.length > 0) {
                                last = parts.join(' ');
                            }
                        }
                        var newUser = User.model({
                            "name.first": first,
                            "name.last": last,
                            email: ctx.body.email || Math.random() + '@groundtruth.com',
                            password: Math.random(),
                            "services.facebook.profileId": ctx.body.id,
                            "services.facebook.isConfigured": true,
                            "services.facebook.avatar": 'https://graph.facebook.com/' + ctx.body.id + '/picture?width=600&height=600'
                        });

                        newUser.save(function (err, result) {
                            keystone.session.signinWithUser(newUser, req, res, function () {
                                req.session.user = newUser;
                                ctx.token = req.sessionID;
                                cb(null, ctx);
                            })
                        })
                    }
                }).catch(function (err) {
                    cb(err, ctx);
                })
        },
        (ctx, cb) =>{
            api_users.populateFriends(req.session.user, req.body.access_token, () => {
				cb(null, ctx)
			})
        }
    ], (err, results) => {
        if (err != null) {
            res.send({ result: 'fail', 'error': err });
        } else res.send({ result: 'success', 'token': results.token });
    })

}
