var keystone = require('keystone'),
	User = keystone.list('User');
var Email = require('keystone-email');

exports = module.exports = function(req, res) {
	
	var view = new keystone.View(req, res),
		locals = res.locals;

	    var host_name = req.headers.host;
	    var req_protocol = req.protocol;
	
	view.on('post', { action: 'forgot-password' }, function(next) {
		
		if (!req.body.email) {
			req.flash('error', "Please enter an email address.");
			return next();
		}

		User.model.findOne().where('email', req.body.email).exec(function(err, user) {
			if (err) return next(err);
			if (!user) {
				req.flash('error', "Sorry, we don't recognise that email address.");
				return next();
			}


			locals.user = user;
            user.resetPasswordKey = keystone.utils.randomString([16,24]);
            locals.link = req_protocol + '://' + host_name + '/reset-password/' + user.resetPasswordKey;
            user.save(function(err) {
                if (err) return next(err);
                new Email('templates/emails/recemail.ejs', { transport: 'mailgun' }).send(locals, {
                    apiKey: 'key-241cd538cdc3f1ccc892189807977f30',
                    domain: 'sandbox5b83c4207cd6453194f69a37021fd0f6.mailgun.org',
                    user: user,
                    link: req_protocol + '://' + host_name + '/reset-password/' + user.resetPasswordKey,
                    subject: 'Reset your Password',
                    to: user.email,
                    from: {
                        name: 'Feedber',
                        email: 'info@feedber.com'
                    }
                }, function(err) {
                    if (err) {
                        console.error(err.message, err.stack);
                        req.flash('error', 'Error sending reset password email!');
                        next();
                    } else {
                        req.flash('success', 'We have emailed you a link to reset the password!');
                        res.redirect('/signin');
                    }
                });
            });
        });
		
	});
	
    view.render('dashboard-new/static-pages/forgot-password.ejs');
	
}
