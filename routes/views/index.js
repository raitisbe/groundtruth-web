var keystone = require('keystone');

exports = module.exports = function (req, res) {
    var engine = require('ejs-locals');
    keystone.init({
        'custom engine': engine,
        'view engine': 'ejs'
	});
	
	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';

	if (req.user){
		res.locals.avatarimage = getProfileAvatarImg(req);
	}

	function getProfileAvatarImg(req){	
		if (req.user.services && req.user.services.facebook && req.user.services.facebook.avatar)
			return req.user.services.facebook.avatar.replace("width=600", "width=30").replace("height=600", "height=30");
		return null;
	}

	// Render the view
	view.render('index.ejs');
};
