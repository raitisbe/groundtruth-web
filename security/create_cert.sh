#!/bin/sh
openssl req -new -key privkey.pem -out new.ssl.csr -config security/openssl_192.168.2.102.cnf
openssl rsa -in  -out key.pem
openssl x509 -in new.ssl.csr -out fullchain.crt -req -signkey key.pem -days 3600 -extensions v3_req -extfile security/openssl_192.168.2.102.cnf
