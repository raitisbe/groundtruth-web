# groundtruth-web

## Project setup
```
npm install
```

### Compiles and hot-reloads frontend for development
```
npm run serve
```

### Compiles and minifies frontend for production
```
npm run build
```

### Runs api server
```
npm run runapi
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
