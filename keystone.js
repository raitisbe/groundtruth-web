// Simulate config options from your production environment by
// customising the .env file in your project's root folder.
require('dotenv').config();

// Require keystone
var keystone = require('keystone');

keystone.init({
	'name': 'GroundTruth',
	'brand': 'GroundTruth',

	'less': 'public',
	static: ['dist', 'public', 'storage'],
	'favicon': 'public/favicon.ico',
	'views': 'dist',
	'view engine': 'pug',
	'file limit': '10mb',
	'auto update': true,
	'session': true,
    'session store': 'mongo',
	'auth': true,
	'user model': 'User',
	port: process.env.HTTP_PORT || 10221,
	'cookie secret': 'edsdsds',
	mongo: process.env.MONGO_URI || "mongodb://localhost/groundtruth",
	ssl: true,
	'ssl key': 'key.pem',
    'ssl cert': 'fullchain.crt',
    'ssl port': process.env.HTTPS_PORT || 10222
});
keystone.import('models');
keystone.set('locals', {
	_: require('lodash'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable,
});
keystone.set('routes', require('./routes'));

keystone.set('nav', {
	users: 'users',
});



keystone.start();
