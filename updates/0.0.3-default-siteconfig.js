var keystone = require('keystone');

exports = module.exports = function(done){
    const SiteConfig = keystone.list('SiteConfig');
    SiteConfig.model.find({})
    .exec().then((items) => {
        if(items == null || items.length ==0){
            let item = new SiteConfig.model();
            item.observations_name = 'Events' ;
            item.observation_name = 'Event';
            item.title = 'GroundTruth';
            item.save(function (err) {
                if (err) {
                    console.error('Error addingsite config to the database:');
                    console.error(err);
                } else {
                    console.log('Added site config to the database.');
                }
                done(err);
            })
        }
    })
}
