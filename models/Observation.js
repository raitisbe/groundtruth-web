var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Observation Model
 * ==========
 */
var Observation = new keystone.List('Observation', { track: true });

Observation.add({
    name: { type: Types.Text},
    description: {type: Types.Textarea},
    observationType: { type: Types.Relationship, ref: 'ObservationType', index: true },
    user: { type: Types.Relationship, ref: 'User', index: true },
    center: {type: Types.Location},
    wkt: {type: Types.Textarea},
    date: { type: Types.Datetime, default: new Date() },
    screenshot: {type: Types.Text},
    image: {type: Types.Text},
    scope: { type: Types.Select, options: 'me, friends, public', default: 'friends' },
    needsChecking: { type: Boolean, default: false }
});

Observation.getDocumentName = function(doc, escape) {
    return doc.name
}

Observation.defaultColumns = 'id, date, name, observationType, description';
Observation.register();
