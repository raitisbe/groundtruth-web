var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Share Model
 * ==========
 */
var Share = new keystone.List('Share', { track: true });

Share.add({
    name: { type: Types.Text},
    user: { type: Types.Relationship, ref: 'User' },
    date: { type: Types.Datetime, default: new Date() },
    screenshot: {type: Types.Text}
});

Share.defaultColumns = 'name, date';
Share.register();
