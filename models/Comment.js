var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Comment Model
 * ==========
 */
var Comment = new keystone.List('Comment', { track: true });

Comment.add({
    user: { type: Types.Relationship, ref: 'User' },
    observation: { type: Types.Relationship, ref: 'Observation', index: true },
    date: { type: Types.Datetime, default: new Date() },
    saw: { type: Boolean, default: false },
    content: { type: Types.Textarea }
});

Comment.defaultColumns = 'observation, date, user, content';
Comment.register();
