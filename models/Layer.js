var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Layer Model
 * ==========
 */
var Layer = new keystone.List('Layer', { track: true });

Layer.add({
    name: { type: Types.Text},
    abstract: {type: Types.Textarea},
    url: { type: Types.Text},
    params: { type: Types.Textarea},
    sourceType: { type: Types.Select, options: 'ArcGIS REST, WMS', default: 'WMS'},
    layerType: { type: Types.Select, options: 'ImageLayer, TileLayer', default: 'ImageLayer'},
    visible: { type: Boolean, default: true }
});

Layer.getDocumentName = function(doc, escape) {
    return doc.name
}

Layer.defaultColumns = 'id, name, sourceType';
Layer.register();
