var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * Vote Model
 * ==========
 */
var Vote = new keystone.List('Vote', { track: true });

Vote.add({
    user: { type: Types.Relationship, ref: 'User' },
    observation: { type: Types.Relationship, ref: 'Observation', index: true },
    date: { type: Types.Datetime, default: new Date() },
    saw: { type: Boolean, default: false },
    value: { type: Number, default: false }
});

Vote.defaultColumns = 'observation, date, user, value';
Vote.register();
