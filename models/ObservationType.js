var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * ObservationType Model
 * ==========
 */
var ObservationType = new keystone.List('ObservationType', { track: true });

ObservationType.add({
    name: { type: Types.Text},
    description: {type: Types.Textarea},
    image: {type: Types.Text},
    voteType: { type: Types.Select, options: 'boolean, numerical', default: 'boolean'}
});

ObservationType.getDocumentName = function(doc, escape) {
    return doc.name
}

ObservationType.defaultColumns = 'id, name';
ObservationType.register();
