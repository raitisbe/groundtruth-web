var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * SiteConfig Model
 * ==========
 */
var SiteConfig = new keystone.List('SiteConfig', { track: true });

SiteConfig.add({   
    observations_name: { type: Types.Text, default: 'Events' },
    observation_name: { type: Types.Text, default: 'Event' },
    title: { type: Types.Text , default: 'GroundTruth'},
    about: { type: Types.Textarea }
});

SiteConfig.defaultColumns = 'title, observations_name, observation_name';
SiteConfig.register();
